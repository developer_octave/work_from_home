﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace work_from_home.Models
{
    public class User
    {
        public string UserId { get; set; }
        public string Name{ get; set; }
        public string Phone{ get; set; }
        public string Email { get; set; }
        public string Designation{ get; set; }
        public string Username{ get; set; }
        public string Password{ get; set; }


        public Role Role { get; set; }
        public string RoleId { get; set; }

        public Department Department{ get; set; }
        public string DeptId{ get; set; }
    }
}

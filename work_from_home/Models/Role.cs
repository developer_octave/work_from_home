﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace work_from_home.Models
{
    public class Role
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace work_from_home.Models
{
    public class DailyReport
    {
        public string ReportId { get; set; }
        public string UserId{ get; set; }
        public Nullable<DateTime> Date{ get; set; }
        public string Subject{ get; set; }
        public string Report{ get; set; }
    }
}

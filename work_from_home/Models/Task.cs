﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace work_from_home.Models
{
    public class Task
    {
        public string TaskId { get; set; }
        public string TaskTo { get; set; }
        public string TaskBy { get; set; }
        public DateTime Date { get; set; }
        public string TaskDetail{ get; set; }
        public string Response{ get; set; }
    }
}

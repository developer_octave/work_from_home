﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace work_from_home.Models
{
    public class Attendance
    {
        public string AttendanceId { get; set; }
        public DateTime Date{ get; set; }
        public DateTime CheckIn{ get; set; }
        public DateTime CheckOut{ get; set; }

        public User User { get; set; }
        public string UserId { get; set; }
    }
}

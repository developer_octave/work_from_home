﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace work_from_home.Models
{
    public class Department
    {
        public string DeptId { get; set; }
        public string DeptName { get; set; }
    }
}
